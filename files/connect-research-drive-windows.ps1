$Logfile = "C:\logs\RD-connect-log.log"

Function LogWrite
{
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

LogWrite("ResearchDrive mount: $env:USERNAME")

$token = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).co_token
$coAddress = (Get-ItemProperty -Path HKLM:\Software\ResearchCloud).co_user_api_endpoint


$headers = @{
    authorization="$token"
}

$RESEARCH_DRIVE_INFO =  (Invoke-RestMethod -Uri $coAddress -Method Get -Headers $headers)

 foreach($item in  $RESEARCH_DRIVE_INFO)
{
    if ($item.username -eq $env:USERNAME){

        LogWrite("Found ResearchDrive info: $env:USERNAME")

        $user = $item.research_drive_secret.username
        $password = $item.research_drive_secret.token
        $url = $item.research_drive_secret.url
        
    }
} 


Function Main { 
    # See if a mapping was already made    
    $Mapping = Test-Path R:
    # If there a mapping then remove it
    if($Mapping)
        {
            net use r: /delete
            LogWrite("Drive mounting removed, moving on to re-mount...")
        }

        LogWrite("Mounting $rdriveURI using username $user...")
    net use r: $url /user:$user $password /Persistent:YES
}

try {
    Main
    # Check if mapping was successful
    $Mapping = Test-Path R:
    if($Mapping)
        {
            LogWrite("ResearchDrive successfully mounted as drive letter R!")
            # Create shortcut using Chocolatey helpers
            Import-Module $env:ChocolateyInstall\helpers\chocolateyInstaller.psm1; Install-ChocolateyShortcut -ShortcutFilePath "$home\Desktop\Research Drive.lnk" -TargetPath R:\
        }
    else 
        {
            LogWrite("Drive mounting failed!")
        }
}
catch {
    LogWrite("$_")
    Throw $_   
}